import org.junit.Test;

import static org.junit.Assert.*;

public class PointTest {
    @Test public void pointsShouldBeAbleToEqual() {
        Point p1 = Point.of(2, 5);
        Point p2 = Point.of(2, 5);

        assertNotSame(p1, p2);
        assertEquals(p1, p2);
        assertEquals(p1.hashCode(), p2.hashCode());
    }

    @Test public void pointsShouldBeNonEqualThenTheyAre() {
        Point p1 = Point.of(2, 5);
        Point p2 = Point.of(3, 4);

        assertNotSame(p1, p2);
        assertNotEquals(p1, p2);
        assertNotEquals(p1.hashCode(), p2.hashCode());
    }
}
