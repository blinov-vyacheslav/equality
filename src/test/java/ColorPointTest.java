import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotSame;

public class ColorPointTest {
    public static int BLACK = 1;
    public static int YELLOW = 2;

    @Test public void pointsShouldBeAbleToEqual() {
        ColorPoint p1 = Point.of(2, 5, BLACK);
        ColorPoint p2 = Point.of(2, 5, BLACK);

        assertNotSame(p1, p2);
        assertEquals(p1, p2);
        assertEquals(p1.hashCode(), p2.hashCode());
    }

    @Test public void pointsShouldBeNonEqualThenTheyAre() {
        ColorPoint p1 = Point.of(2, 5, BLACK);
        ColorPoint p2 = Point.of(3, 4, YELLOW);

        assertNotSame(p1, p2);
        assertNotEquals(p1, p2);
        assertNotEquals(p1.hashCode(), p2.hashCode());
    }
}
