import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static ColorPointTest.*;
import static org.junit.Assert.*;

public class TaskTest {
    @Test public void pointsHaveReflecsiveEquals() {
        Point p1 = Point.of(1, 4);
        assertEquals(p1, p1);
    }

    @Test public void pointsHaveSimmetricalEquals() {
        Point p1 = Point.of(1, 4);
        Point p2 = Point.of(1, 4, BLACK);

        assertNotSame(p1, p2);
        assertEquals(p1, p2);
        assertEquals(p2, p1);
    }

    @Test public void pointsHaveTransitiveEquals() {
        Point p1 = Point.of(1, 4);
        Point p2 = Point.of(1, 4, BLACK);
        Point p3 = Point.of(1, 4);

        assertNotSame(p1, p2);
        assertNotSame(p1, p3);
        assertNotSame(p2, p3);

        // if p1 == p2
        assertEquals(p1, p2);
        // and p2 == p3
        assertEquals(p2, p3);
        // then p1 == p3
        assertEquals(p1, p3);
    }

    @Test public void colorPointFollowsLiskovSubstitutionPrinciple() {
        Point p1 = Point.of(1, 4);
        Point p2 = Point.of(1, 4, BLACK);

        assertNotSame(p1, p2);
        assertEquals(p1, p2);

        Set<Point> points = new HashSet<Point>();
        assertTrue(points.add(p1));
        assertTrue(points.contains(p1));
        assertTrue(points.contains(p2));
        assertFalse(points.add(p2));

        points.remove(p1);
        assertTrue(points.add(p2));
        assertTrue(points.contains(p2));
        assertTrue(points.contains(p1));
        assertFalse(points.add(p1));
    }

    @Test public void colorPointsCreateValidSet() {
        Point p1 = Point.of(3, 0, BLACK);
        Point p2 = Point.of(3, 0, YELLOW);

        Set<Point> points = new HashSet<Point>();
        assertTrue(points.add(p1));
        assertTrue(points.contains(p1));
        assertFalse(points.contains(p2));
        assertTrue(points.add(p2));
        assertEquals(points.size(), 2);
    }
}
