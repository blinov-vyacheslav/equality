public class ImplementationMissing extends RuntimeException {
    public ImplementationMissing() {
        super("No implementation provided");
    }
}
