public class Point {
    public final int x;
    public final int y;

    public static ColorPoint of(int x, int y, int color) {
        return new ColorPoint(x, y, color);
    }

    public static Point of(int x, int y) {
        return new Point(x, y);
    }

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }


    @Override public boolean equals(Object other) {
        throw new ImplementationMissing();
    }
}
