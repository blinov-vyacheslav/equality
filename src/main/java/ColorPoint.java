public class ColorPoint extends Point {
    public final int color;

    public ColorPoint(int x, int y, int color) {
        super(x, y);
        this.color = color;
    }

    @Override public boolean equals(Object other) {
        throw new ImplementationMissing();
    }
}
